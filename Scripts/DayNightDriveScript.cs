﻿/// <summary>
/// Day night drive script.
/// 
/// Equations taken from the following sources:
/// http://www.itacanet.org/the-sun-as-a-source-of-energy/part-3-calculating-solar-angles/
/// 
/// </summary>

using UnityEngine;
using System;

[System.Serializable]
public struct CustomDateTime {

	public int year;

	[Range (1, 12)] 
	public int month;

	[Range (1, 31)] 
	public int day;

	[Range (0, 23)] 
	public int hour;

	[Range (0, 59)] 
	public int min;
}

[Serializable]
public class DayNightDriveScript : MonoBehaviour {

	[Header("Lights")]
	[SerializeField] Light sunLight;
	[SerializeField] Light moonLight;

	[Header("Location coordinates")]
	public float latitude = 49f, longitude = 138f;

	[Header("Time settings")]
	[SerializeField]
	[Tooltip("The time scale. Number of realtime seconds in an hour")]
	float hourTimeScale = 1;

	[SerializeField] 
	[Tooltip("Number of hours in a day")]
	float dayDuration = 24f;

	[SerializeField] 
	[Tooltip("Number of days in a year")]
	float yearDuration = 365.25f;

	[SerializeField] 
	[Tooltip("Whether we use the current system time to start the clock, or a custom time")]
	bool startWithSystemTime = false;

	[SerializeField] CustomDateTime customDateTime;

	private float sunDrive = 0.0f, 
					clockwork = 0f, 
					sunDeclination = 0,
					sunShift = 0,
					sunLength = 0,
					dayNightFlip = -1,
					sunPitch = 0,
					sunYaw = 0;

	#region Sun Declination helper properties

	bool IsPolar {
		get {
			return ((90 - (latitude + sunDeclination)) < 0f || (90 - (latitude - sunDeclination)) < 0f);
		}
	}

	private float EquatorFlip {
		get {
			if (((90 - latitude) + sunDeclination) > 90) {
				return -1;
			} else {
				return 1;
			}
		}
	}

	private float MaxSunDeclination {
		get {
			return 23.45f * Mathf.PI / 180f * Mathf.Sin(2 * Mathf.PI * ( (284f + Now.DayOfYear) / 36.25f )) * Mathf.Sign(latitude);
		}
	}

	#endregion

	public static DateTime Now {
		get;
		private set;
	}

	public float Clockwork
	{
		get {
			return (float)Now.TimeOfDay.TotalHours;
		}
	}

	void Start () {
		if (startWithSystemTime) {
			Now = DateTime.Now;
		} else {
			Now = new DateTime (customDateTime.year, customDateTime.month, customDateTime.day, customDateTime.hour, customDateTime.min, 0);
		}

		clockwork = (float)Now.TimeOfDay.TotalHours;
		sunDrive = (clockwork / 24f) * dayDuration;
	}

	void Update () {

		var modifiedDeltaTime = Time.deltaTime / hourTimeScale;
		Now = Now.AddHours (modifiedDeltaTime);

		sunDrive += modifiedDeltaTime;

		var dayCounter = (modifiedDeltaTime / dayDuration) * 24;

		clockwork = (dayCounter + clockwork) % 24; 

		UpdateSunTrajectory ();

		if (sunDrive >= dayDuration) {
			sunDrive = 0f;
		}
	}

	void UpdateSunTrajectory() {

		float workingDeclination = (((clockwork / 24f) + Now.DayOfYear + 10) / yearDuration) * 2 * Mathf.PI;
		workingDeclination = Mathf.Cos (workingDeclination) * MaxSunDeclination;
		sunDeclination = workingDeclination;

		float sunHalfDay = 0f, sunHalfNight = 0f, pitchLatitudeSqr = 0f;

		if (IsPolar) {
			sunHalfDay = sunHalfNight = (dayDuration * 0.25f);
			pitchLatitudeSqr = 90f - Mathf.Abs (latitude);
		}
		else {
			pitchLatitudeSqr = 90f - Mathf.Abs(latitude - (sunDeclination * dayNightFlip));

			sunHalfDay = Mathf.Tan (latitude * Mathf.Deg2Rad) * Mathf.Tan (sunDeclination * -1 * Mathf.Deg2Rad);
			sunHalfDay = Mathf.Acos (sunHalfDay) / ((360f * Mathf.Deg2Rad) / dayDuration);

			sunHalfNight = Mathf.Tan (latitude * Mathf.Deg2Rad) * Mathf.Tan (sunDeclination * Mathf.Deg2Rad);
			sunHalfNight = Mathf.Acos (sunHalfNight) / ((360f * Mathf.Deg2Rad) / dayDuration);
		}

		float endOfDay = sunHalfNight + (sunHalfDay * 2);

		if (sunDrive > sunHalfNight && sunDrive <= endOfDay) {
			// Day
			dayNightFlip = 1;
			sunShift = dayDuration / 2f;
			sunLength = sunHalfDay;

		} else {
			//Night
			dayNightFlip = -1;
			sunShift = sunDrive > endOfDay ? dayDuration : 0f;
			sunLength = sunHalfNight;

		}

		// Calculate Sun Pitch
		var sqrtBit = 90f - Mathf.Abs (latitude - (sunDeclination * dayNightFlip));
		var topBit = Mathf.Pow(((sunDrive - sunShift) / sunLength) * Mathf.Sqrt (pitchLatitudeSqr), 2);
		sunPitch = ((topBit * dayNightFlip) - (dayNightFlip * sqrtBit));

		//  Calculate Sun Yaw
		sunYaw = (EquatorFlip * ((sunDrive * 360) / dayDuration)) + ((1 - EquatorFlip) * 90);

		sunLight.transform.rotation = Quaternion.identity;
		sunLight.transform.Rotate (new Vector3 (sunPitch + 180, sunYaw, 0), Space.Self); // We add 180 degress to the pitch as Unity has a different axis setup to UE4
	}
}
