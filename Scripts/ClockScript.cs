﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class ClockScript : MonoBehaviour {

	private const float
	hoursToDegrees = 360f / 12f,
	minutesToDegrees = 360f / 60f,
	secondsToDegrees = 360f / 60f;

	[SerializeField] Transform hourHand = null, minuteHand = null, secondHand = null;
	[SerializeField] Text digitalText;

	public bool useSmoothHands;
	public bool showSeconds;

	void Start() {
		secondHand.gameObject.SetActive (showSeconds);
	}

	void Update() {

		DateTime time = DayNightDriveScript.Now;

		if (useSmoothHands) {
			TimeSpan timespan = time.TimeOfDay;
			hourHand.localRotation = Quaternion.Euler(
				0f, 0f, (float)timespan.TotalHours * -hoursToDegrees);
			minuteHand.localRotation = Quaternion.Euler(
				0f, 0f, (float)timespan.TotalMinutes * -minutesToDegrees);
			secondHand.localRotation = Quaternion.Euler(
				0f, 0f, (float)timespan.TotalSeconds * -secondsToDegrees);
		}
		else {
			hourHand.localRotation =
				Quaternion.Euler(0f, 0f, time.Hour * -hoursToDegrees);
			minuteHand.localRotation =
				Quaternion.Euler(0f, 0f, time.Minute * -minutesToDegrees);
			secondHand.localRotation =
				Quaternion.Euler(0f, 0f, time.Second * -secondsToDegrees);
		}

		digitalText.text = time.ToString ("HH:mm:ss\nddd dd/MM/yy");

	}
}
