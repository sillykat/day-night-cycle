﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DayNightDriveScript))]
public class DayNightDriveEditor : Editor {
	
	//private SerializedObject m_Object;

	void OnEnable() {
		//m_Object = new SerializedObject(target);
	}

	public override void OnInspectorGUI()
	{
		DayNightDriveScript myTarget = (DayNightDriveScript)target;

		EditorGUILayout.LabelField("Current Date & Time:", DayNightDriveScript.Now.ToString("G"));
		EditorGUILayout.LabelField("Total Days:", DayNightDriveScript.Now.TimeOfDay.TotalDays.ToString());
		EditorGUILayout.LabelField("Clockwork:", myTarget.Clockwork.ToString());

		DrawDefaultInspector ();

		if(GUI.changed)
		{
			EditorUtility.SetDirty( target );
		}
	}
}
